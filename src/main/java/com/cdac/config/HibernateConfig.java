package com.cdac.config;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@PropertySource(value = "classpath:hibernate.properties")
@Configuration

/*
 * Enable Transaction Management
 * 
 * Replacement for :
 * 
 * <tx:annotation-driven transaction-manager="transactionManager" />
 * 
 */
@EnableTransactionManagement
public class HibernateConfig {

	@Value("${db.url}")
	private String url;

	@Value("${db.driver}")
	private String driver;

	@Value("${db.username}")
	private String username;

	@Value("${db.password}")
	private String password;

	@Autowired
	private Environment environment;

	/*
	 * Hibernate Data Source Configuration
	 * 
	 * => Create Bean with id=mysqlDataSource, id of bean is same as method name.
	 * 
	 * => Replacement for :
	 * 
	 * <bean name="dataSource"
	 * class="org.springframework.jdbc.datasource.DriverManagerDataSource">
	 * 
	 * <property name="driverClassName" value="com.mysql.cj.jdbc.Driver" />
	 * 
	 * <property name="url" value="jdbc:mysql://localhost:3306/sh09" />
	 * 
	 * <property name="username" value="root" />
	 * 
	 * <property name="password" value="root" />
	 * 
	 * </bean>
	 */
	@Bean
	public DriverManagerDataSource mysqlDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(driver);
		dataSource.setUrl(url);
		dataSource.setUsername(username);
		dataSource.setPassword(password);

		return dataSource;
	}

	/*
	 * Hibernate Session Factory Configuration
	 * 
	 * Replacement for :
	 * 
	 * <bean id="sessionFactory"
	 * class="org.springframework.orm.hibernate5.LocalSessionFactoryBean">
	 * 
	 * <property name="dataSource" ref="dataSource"></property>
	 * 
	 * <property name="configLocation" value="classpath:hibernate.cfg.xml" />
	 * 
	 * <!-- If we want to configure hibernate properties in this file only then we
	 * have below option/syntax -->
	 * 
	 * <!--
	 * 
	 * <property name="hibernateProperties">
	 * 
	 * <props>
	 * 
	 * <prop key="hibernate.dialect"> org.hibernate.dialect.MySQL8Dialect</prop>
	 * 
	 * <prop key="hibernate.show_sql"> true </prop>
	 * 
	 * </props>
	 * 
	 * </property>
	 * 
	 * -->
	 * 
	 * <property name="packagesToScan" value="com.cdac.entity" /> </bean>
	 * 
	 */
	@Bean
	public LocalSessionFactoryBean sessionFactoryBean() {
		LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();

		sessionFactoryBean.setDataSource(this.mysqlDataSource());
		sessionFactoryBean.setPackagesToScan("com.cdac");

		Properties properties = new Properties();

		properties.setProperty("hibernae.dialect", environment.getRequiredProperty("hibernate.dialect"));
		properties.setProperty("hibernae.show_sql", environment.getRequiredProperty("hibernate.show_sql"));

		sessionFactoryBean.setHibernateProperties(properties);

		return sessionFactoryBean;
	}

	/*
	 * Replacement for:
	 * 
	 * <bean id="transactionManager"
	 * class="org.springframework.orm.hibernate5.HibernateTransactionManager">
	 * 
	 * <property name="sessionFactory" ref="sessionFactory" />
	 * 
	 * </bean>
	 */
	@Autowired
	@Bean
	public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {

		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(sessionFactory);

		return transactionManager;
	}

}