package com.cdac.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/*
 * Replacement for : web.xml
 */
public class MyWebMvcDispatcherServletInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * Replacement for :
	 * 
	 * <servlet>
	 * 
	 * <servlet-name>dispatcherServlet</servlet-name>
	 * 
	 * <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-
	 * class>
	 * 
	 * <init-param>
	 * 
	 * <param-name>contextConfigLocation</param-name>
	 * 
	 * <param-value>/WEB-INF/dispatcherServletBeans.xml</param-value>
	 * 
	 * </init-param>
	 * 
	 * <load-on-startup>1</load-on-startup>
	 * 
	 * </servlet>
	 */
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] { SpringMvcConfig.class };
	}

	/*
	 * 
	 * Replacement for :
	 * 
	 * <servlet-mapping>
	 * 
	 * <servlet-name>dispatcherServlet</servlet-name>
	 * 
	 * <url-pattern>/</url-pattern>
	 * 
	 * </servlet-mapping>
	 */
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}

}
