package com.cdac.config;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

/*
 * SpringMvcConfig.java 
 * 
 * Replacement for : dispatcherServletBeans.xml
 * 
 */

/*
 * @Configuration : To tell Spring that it's a configuration class and create its bean accordingly
 * 
 */
@Configuration

/*
 * @ComponentScan
 * 
 * Replacement for : <context:component-scan base-package="com.cdac" />
 */
@ComponentScan(basePackages = "com.cdac") // Scan for @Configuration and Stereotype Annotations enable Classes

/*
 * @EnableWebMvc
 * 
 * Replacement for : <mvc:annotation-driven />
 */
@EnableWebMvc

/*
 * Import Hibernate Configuration
 * 
 * Replacement for : <import resource="classpath:hibernateBeans.xml" />
 * 
 * Note > The importing of other configuration classes are OPTIONAL. Because we
 * have @Configuration on HibernateConfig class and @ComponentScan will scan for
 * all @Configuration classes and @Bean to initialize bean in Spring Container.
 * 
 * If our Configuration class is located other than base-package then we need to
 * import it.
 * 
 */
@Import({ HibernateConfig.class }) // Optional if HibernateConifig Class is under base package and Annotated with
									// @Configuration
public class SpringMvcConfig implements WebMvcConfigurer {

	/*
	 * ViewResolver Configuration
	 * 
	 * => Create Bean with id=viewResolver, id of bean is same as method name.
	 * 
	 * => Replacement for :
	 * 
	 * <bean id="viewResolver"
	 * class="org.springframework.web.servlet.view.UrlBasedViewResolver">
	 * 
	 * <property name="viewClass"
	 * value="org.springframework.web.servlet.view.JstlView" />
	 * 
	 * <property name="prefix" value="/WEB-INF/views/" />
	 * 
	 * <property name="suffix" value=".jsp" />
	 * 
	 * </bean>
	 */
	@Bean
	public UrlBasedViewResolver viewResolver() {
		UrlBasedViewResolver viewResolver = new UrlBasedViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		return viewResolver;
	}

	/*
	 * Static Resource Handler Configuration
	 *
	 * => addResourceHandlers(ResourceHandlerRegistry registry)
	 * 
	 * => Replacement for :
	 * 
	 * <mvc:resources location="/WEB-INF/static/css/" mapping="/css/**" />
	 * <mvc:resources location="/WEB-INF/static/js/" mapping="/js/**" />
	 * <mvc:resources location="/WEB-INF/static/images/" mapping="/images/**" />
	 * 
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/css/**").addResourceLocations("/WEB-INF/static/css/");
		registry.addResourceHandler("/js/**").addResourceLocations("/WEB-INF/static/js/");
		registry.addResourceHandler("/images/**").addResourceLocations("/WEB-INF/static/images/");
	}

	/*
	 * Create Bean with id=exceptionResolver, id of bean is same as method name
	 * 
	 * Replacement for :
	 * 
	 * <bean class=
	 * "org.springframework.web.servlet.handler.SimpleMappingExceptionResolver">
	 * 
	 * <property name="exceptionMappings">
	 * 
	 * <props>
	 * 
	 * <prop
	 * key="com.cdac.exception.CustomException">exceptions/specific_custom_exception
	 * </prop>
	 * 
	 * <prop key="java.io.FileNotFoundException">exceptions/
	 * specific_filenotfound_exception</prop>
	 * 
	 * </props>
	 * 
	 * </property>
	 * 
	 * <property name="defaultErrorView"
	 * value="exceptions/general_exception"></property> </bean>
	 */

	@Bean
	public SimpleMappingExceptionResolver exceptionResolver() {
		SimpleMappingExceptionResolver exceptionResolver = new SimpleMappingExceptionResolver();
		Properties propertis = new Properties();

		propertis.setProperty("java.lang.RuntimeException", "special_runtime_exception");
		exceptionResolver.setExceptionMappings(propertis);

		exceptionResolver.setDefaultErrorView("general_exception");

		return exceptionResolver;
	}

	/*
	 * FileUpload Configuration
	 * 
	 * => Create Bean with id=multipartResolver, id of bean is same as method name.
	 * 
	 * => Replacement for :
	 * 
	 * <bean id="multipartResolver"
	 * class="org.springframework.web.multipart.commons.CommonsMultipartResolver">
	 * 
	 * <property name="maxUploadSize" value="-1"/>
	 * 
	 * </bean>
	 */

	@Bean
	public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver resolver = new CommonsMultipartResolver();
		resolver.setMaxUploadSize(-1);
		return resolver;
	}

}
