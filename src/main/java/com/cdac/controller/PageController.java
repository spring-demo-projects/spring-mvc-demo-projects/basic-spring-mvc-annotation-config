package com.cdac.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController {

	@GetMapping(value = "/", produces = "text/html")
	public String showIndexPage() {

		return "index";

	}

}
